
// #include <Keyboard.h>
#include <Wire.h>
#include <SPI.h>  //serial controller libraries
#include <LoRa.h>


// ========= G L O B A L   V A R I A B L E   S E C T I O N ========================

// Analog Pin Assignments 
int R_directn = A0;     // A0 reads potentiometer that measures current direction of rotor 


// Digital Pin Assignments
byte relay_cw   = 5;     // D5 turns rotor clockwise
byte relay_ccw  = 6;     // D6 turns rotor counterclockwise
byte relay_beam = 4;     // D4 connects transm line to beam
byte relay_wire = 3;     // D3 connects transmission line to wire
//int speed_cw = 0;
//int speed_ccw = 0;

//String incoming = "";

struct packetData  
{
  bool R12 = 0;         // D7 turns rotor clockwise
  bool R34 = 0;       // D8 turns rotor counterclockwise
  bool R5  = 0;  
  bool R6  = 0;
  bool R7  = 0;
  bool R8  = 0;
  int R_cur_direct = 0;
  bool RS12 = 0;         // D7 turns rotor clockwise
  bool RS34 = 0;       // D8 turns rotor counterclockwise
  bool RS5  = 0;  
  bool RS6  = 0;
  bool RS7  = 0;
  bool RS8  = 0;
};

packetData Paket;

// Relay variables
/*  Relay pins are controlled with D7 (clockwise) and D8 (ccw).
 *
 *  Connections: D7 to R1 (IN1) and R2 (IN3); D8 to R3 (IN2) and R4 (IN4).
 *d
    D7 switches relays R1 and R2, sending + and - through normally open (NO) connections
    to the rotor when D8 = high.

    R1 also supplies R3 through NC, and R2 supplies R4 thorough NC.
    The connections to R3 and R4 are interrupted when D7 = high.

    When D8 is switched high, then R3 and R4 close, sending:
     + from R3 (NO, high, via R1 NC, low) to the output of R2 (NO, low), and
     - from R4 (NO, high, via R2 NC, low) to the output of R1 (NO, low)

    Connecting the jumpers from R1 to R3 and R2 to R4 to NC ensures that, if D8 and D9 are
    both high during intialization, a shortage won't occur in the rotor's power supply.
    Thus,this provides a safe switching arrangement.
*/



/* LORA Board Connections  !!! 3.3 V !!!
 *
 *  En/NSS - D10    -- In SPI communication a chip select/enable pin will help to activate 
                       the slave. In the SPI, the NSS pin will perform that function
 *  G0/DIO0 - D2    -- Interrupt
 *  SCK   -  D13    -- 
 *  SCK   -  D13    -- for the clock pulse during the SPI communication
 *  MISO  -  D12    -- MISO = Master in and Slave out; this pin will send data to Arduino 
 *  MOSI  -  D11    -- MOSI = Master out Slave In; this pin will receive the data from Arduino
 *  RST   -   D9    -- Reset 
 */

//const byte csPin = 7;          // LoRa radio chip select
//const byte resetPin = 9;       // LoRa radio reset
//const byte irqPin = 1;         // change for your board; must be a hardware interrupt pin

//define the digital pins used by the LoRa transceiver module
#define SCK  13
#define MISO 12
#define MOSI 11
#define SS    7
#define RST   9
#define DIO0  2

#define BAND  433E6
#define SPIFreq 8E6

//OLED pins
//#define OLED_SDA 4
//#define OLED_SCL 5 
// #define OLED_RST 16
//#define SCREEN_WIDTH 128 // OLED display width, in pixels
//#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// String outgoing;              // outgoing message

// byte msgCount = 0;            // count of outgoing messages
// byte localAddress = 0xBB;     // address of this device
// byte destination = 0xFF;      // destination to send to
// long lastSendTime = 0;        // last send time
// int interval = 200;          // interval between sends


int send_delay = 100;     // interval between sends
int packetSize = 0;



// ================= L O R A   F U N C T I O N   S E C T I O N ===================

// LORA LOOPS -- Send and Receive
// https://github.com/sandeepmistry/arduino-LoRa/blob/master/API.md 

void displayStructure()
{
  Serial.println("R1 : " + String(Paket.R12));
  Serial.println("R3 : " + String(Paket.R34));
  Serial.println("R5 : " + String(Paket.R5));
  Serial.println("R6 : " + String(Paket.R6));
  Serial.println("R7 : " + String(Paket.R7));
  Serial.println("R8 : " + String(Paket.R8));
  Serial.println("R_cur_direct : " + String(Paket.R_cur_direct));
  Serial.println();
}


void sendMessage(void) 
{
  Serial.println("Entered Send loop; this is what's being sent: ");
  displayStructure();                   // show what's in the variable structure

  LoRa.beginPacket();                   // start packet
  LoRa.write((uint8_t*)&Paket, sizeof(Paket)); // cast Paket to uint8_t [unsigned char]
  LoRa.endPacket();                     // finish packet and send it

  Serial.println("Sent message of size " + String(sizeof(Paket)));
  Serial.write((uint8_t*)&Paket, sizeof(Paket)); // cast Paket to uint8_t [unsigned char]
  Serial.println("Coming out ouf Send Loop");
  Serial.println();
}
 

 
void onReceive(int packetSize) 
{  
  uint8_t incomingLength = 0;
  String incoming; 
  
  Serial.println("Entered Receive loop; this is what Struct is: ");
  displayStructure();                   // show what's in the variable structure

  packetSize = LoRa.parsePacket();
  Serial.println("Receiving Package; size : " + String(packetSize)); delay(100);
  
 
  if (packetSize) // Only read if there is some data to read..
  {
      LoRa.readBytes((uint8_t *)&Paket, packetSize);
      Serial.println("LoRa Reading Packet"); delay(100);

  }

  if (incomingLength != incoming.length())    // check length for error
  {
    Serial.println("error: message length does not match length");
    return;                             // skip rest of function
  }

  // print details to serial interface:
  Serial.println("Received package" );
  Serial.println("RSSI: " + String(LoRa.packetRssi()));
  Serial.println("Snr: "  + String(LoRa.packetSnr()));
  Serial.println("Packet Size: " + String(packetSize));
  Serial.println("This is what Struct looks like: ");
  displayStructure();                   // show what's in the variable structure
  Serial.println("Coming out ouf Receive Loop");
  Serial.println();
}




// =============== M A I N   S E T U P   S E C T I O N =====================


void setup() 
{
  Serial.begin(9600);
 
  Serial.println("Serial OK"); delay(500);

  pinMode(relay_cw, OUTPUT);        // digital pin assignment as output pin
  digitalWrite(relay_cw, HIGH);     // switches signal  off
  pinMode(relay_ccw, OUTPUT);       // digital pin assignment as output pin
  digitalWrite(relay_ccw, HIGH);    // switches signal  off

  Serial.println("Digital pins Set"); delay(500);
  delay(5000);

  // SPI.begin(SCK, MISO, MOSI, SS);
  SPI.begin();
  LoRa.setSPIFrequency(SPIFreq);       // sets frequency of SPI interface; default = 8MHz
  Serial.println("SPI started"); delay(500);

  //setup LoRa transceiver module
  LoRa.setPins(SS, RST, DIO0); // set SS, reset, IRQ pin
    // ss - new slave select pin to use, defaults to 10
    // reset - new reset pin to use, defaults to 9
    // dio0 - new DIO0 pin to use, defaults to 2. Must be interrupt capable via attachInterrupt(...).
 
  // LoRa.setSpreadingFactor(8);           // ranges from 6-12,default 7 see API docs
  LoRa.setTxPower(5);                   // sets tx power; default = 17dB; range = 2, 20
  // LoRa.setSyncWord(51153);

  Serial.println("LORA pins & power set"); delay(500);

  if (!Serial)
  {
    Serial.println("LoRa Duplex Init"); delay(500);
    while(true);
  }

  byte cnt = 0;
  cnt = LoRa.begin(BAND);
  Serial.println("LoRa Begin Command Issued; Result: " + String(cnt)); delay(500);
 
  while ((cnt == 0))
  {
    Serial.println("LoRa Not Started; Result: " + String(cnt)); delay(500);
    digitalWrite(9,LOW);
    delay(200);
    LoRa.begin(BAND); delay(300);
    digitalWrite(9, HIGH);
    cnt = LoRa.begin(BAND);
  }
  Serial.println("LoRa Success!"); delay(1000);

}

// =============== M A I N   C O D E   S E C T I O N =====================


void loop() 
{

  // Read rotor turn knob (read through A0) value and set to 60 degree equivalent

  onReceive(LoRa.parsePacket());    // parse for a packet, and call onReceive with the result:

  Paket.R_cur_direct = analogRead(R_directn);
  Serial.println("R_cur_direct : " + String(Paket.R_cur_direct)); delay(200);

  delay(10);

  onReceive(LoRa.parsePacket()); 

  if ((Paket.R12 = 0)) { relay_cw   = HIGH; } else { relay_cw   = LOW; };    // high = off; low = on
  if ((Paket.R34 = 0)) { relay_ccw  = HIGH; } else { relay_ccw  = LOW; };    // high = off; low = on
  // if (Parket.R5  = 0) { relay_beam = HIGH } else { relay_beam = LOW }    // high = off; low = on
  // if (Parket.R6  = 0) { relay_wire = HIGH } else { relay_wire = LOW }    // high = off; low = on
  // if (Parket.R7 = 0) { relay_cw = HIGH } else {relay_cw = LOW}     // high = off; low = on
  // if (Parket.R8 = 0) { relay_cw = HIGH } else {relay_cw = LOW}     // high = off; low = on
  delay(50);
}
