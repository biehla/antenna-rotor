
// #include <Keyboard.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <SPI.h>  //serial controller libraries
#include <LoRa.h>

volatile boolean haveData = false;      //TEMP
 

/// ============== G L O B A L   V A R I A B L E   S E C T I O N ========================

int new_direct  =  60;     // stores the value for the target direction in degrees [0, 360]
int cur_direct =   60;
int R_new_direct = A0;     // A0 reads potentiometer that sets new direction
int delay_val  =  500;

char celeste[3];           // variable that translates degrees into celestial directions [N,S,E,W]


/* Relay variables
   
  Relay pins are controlled with D7 (clockwise) and D8 (ccw).
 
  Connections: D7 to R1 (IN1) and R2 (IN3); D8 to R3 (IN2) and R4 (IN4).
 
  D7 switches relays R1 and R2, sending + and - through normally open (NO) connections
  to the rotor when D8 = high.

  R1 also supplies R3 through NC, and R2 supplies R4 thorough NC.
  The connections to R3 and R4 are interrupted when D7 = high.

  When D8 is switched high, then R3 and R4 close, sending:
  + from R3 (NO, high, via R1 NC, low) to the output of R2 (NO, low), and
  - from R4 (NO, high, via R2 NC, low) to the output of R1 (NO, low)

  Connecting the jumpers from R1 to R3 and R2 to R4 to NC ensures that, if D8 and D9 are
  both high during intialization, a shortage won't occur in the rotor's power supply.
  Thus,this provides a safe switching arrangement.
*/


struct packetData  
{
	bool R12 = 0;			     // R## =  commands to Rotor
	bool R34 = 0;				
	//bool R5  = 0;  
	bool R6  = 0;
	bool R7  = 0;
	//bool R8  = 0;
	int R_cur_direct = 0;
  bool RS12 = 0;         // RS## = Switch measurements from Rotor
  bool RS34 = 0;       
  //bool RS5  = 0;  
  bool RS6  = 0;
  bool RS7  = 0;
  //bool RS8  = 0;
};

packetData Paket;

/* LORA Board Connections  !!! 3.3 V !!!
  *
  *  En/NSS - D10
  *  G0/DIO0 - D2
  *  SCK   -  D13
  *  MISO  -  D12
  *  MOSI  -  D11
  *  RST   -   D9
*/

//define the digital pins used by the LoRa transceiver module
#define SCK  13
#define MISO 12
#define MOSI 11
#define SS    7             // LoRa radio chip select
#define RST   9             // LoRa radio reset
#define DIO0  2

#define BAND  433E6
#define SPIFREQ 8E6 

int send_delay = 100;       // interval between sends
int packetSize = 0;


/*  LCD Connections 5V
 *  SDA - A4 - located close to processor
 *  SCL - A5 - located away from processor
 */

LiquidCrystal_I2C lcd(0x27, 20, 4);  // // set the LCD address to 0x27 for a 16 chars and 2 line display



/// ============== R O T A T I O N   F U N C T I O N   S E C T I O N ================


// THIS FUNCTION CALCULATES THE DIRECTION [degrees] IN TERMS OF N/S E/W

void celeste_fn(int x) {

  if ((x >= 315) | (x < 23)) {
    strcpy(celeste, "N");
  }
  else if (x <  68) {
    strcpy(celeste, "NE");
  }
  else if (x < 113) {
    strcpy(celeste, "E");
  }
  else if (x < 158) {
    strcpy(celeste, "SE");
  }
  else if (x < 203) {
    strcpy(celeste, "S");
  }
  else if (x < 248) {
    strcpy(celeste, "SW");
  }
  else if (x < 293) {
    strcpy(celeste, "W");
  }
  else if (x < 315) {
    strcpy(celeste, "NW");
  }
}



/// ================== R O T O R   F U N C T I O N S ======================

// CURRENT ROTOR POSISION

/* Board uses an H bridge that compares the rotor's rotary resistor (R2) to a resistor
   of known value (R1).
   Resistance values of the rotor always linearly correspond to the same turning position.
   Min: -13º = 26Ω; 0º = 67Ω; 180º = 250Ω; 360º = 435Ω; Max: 405º = 480Ω
   Degrees are in azithmutal values.
*/

void cur_direct_fn() 
{
  /* 
    comparison resistor = 670 ohm; rotor resistor = R2; 
    determine current direction; 0 deg = 67 Ohm, 405 deg = 480 Ohm (linear); could be done by
    linear scaling: cur_direct = round(round(R2) * (435 - 67) / 360 - 70); 
    here we use the map functionw whic maps 621 to 928 ohm in R2 onto 0 to 360 degrees
    after reading it from the rotor control unit  
  */

  onReceive(LoRa.parsePacket());                        // receive lora package and extract R value
  
  cur_direct = map(Paket.R_cur_direct, 621, 928, 0, 360);     // maps the value onto azumuth [degrees]
  degree_fn();                 						  		  // makes sure it's between 0 and 360 degrees

}



// THIS FUNCTIONS READS THE NEW DIRECTION

/* 
  Reads pin A0 [0, 1023] to detect position of knob potentiometer on the front face
  of the box; maps result onto scale from 0 to 360 degrees
*/

void new_direct_fn() 
{
  new_direct = map(analogRead(R_new_direct), 18, 950, 0, 360);
}



// THIS FUNCTION CORRECTS THE DIRECTION [DEGREES] IF OUT OF BOUNDS [0.360]

void degree_fn() 
{
  if (cur_direct > 400) cur_direct = cur_direct - 360;
  if (cur_direct <  0 ) cur_direct = cur_direct + 360;
}



// THIS FUNCTION WRITES THE NEW AND CURRENT DIRECTION ONTO THE LCD DISPLAY

void lcd_display() 
{
  // lcd.clear();  //Clean the screen

  char Strup[17];

  celeste_fn(new_direct);
  sprintf(Strup, "Pos Set: %4d %2s", new_direct, celeste);
  lcd.setCursor(0, 0);
  lcd.print(Strup);

  celeste_fn(cur_direct);
  sprintf(Strup, "Pos Cur: %4d %2s", cur_direct, celeste);
  lcd.setCursor(0, 1);
  lcd.print(Strup);

  delay(delay_val / 10); //Delay used to give a dynamic effect
}



// THIS FUNCTION TURNS THE ROTOR

void turn_rotor_fn() 
{
  Serial.println("We are now in the Rotor Turn Function");
 
  while (abs(cur_direct - new_direct) > 3) 
  {
    Serial.println("turny turn");
    // Clockwise turn

    if (cur_direct > new_direct) // CW TURN
    {
      Paket.R12 = 1;
      Paket.R34 = 0;
      delay(send_delay);
      sendMessage();
    } 
    else   // CCW turn
    {
      Paket.R12 = 0;
      Paket.R34 = 1;
      sendMessage();
      delay(send_delay);
    }

    cur_direct_fn();
    new_direct_fn();
    lcd_display();
  }

  Paket.R12 = 0;
  Paket.R34 = 0;
  sendMessage();
  delay(send_delay);
}


/// =========== A N T E N N A   S W I T C H I N G   S E C T I O N ==============




/// ================ L O R A   F U N C T I O N   S E C T I O N ==================

/* 
LORA sends codes to switch on / off one of the eight relays, according to the following table:

           LOW   HIGH   NOTE
Relays 1,2: 10	  11	Switched by Pin 7:    10 = off; 11 = Clockwise
Relays 3,4:	30	  31	Switched by Pin 8:    30 = off; 31 = Counter clockwise 

Relay 5:    50    51    Beam Antenna
Relay 6:    60    61    Wire Antenna
Relay 7:    70    71    Ground 

Relays 1-4 control the rotation of the rotor; relays 5-8 switch between two antennas and ground. 

Lora receives packages indicating the current direction of the Rotor (measured as a resistance
so all corrections can take place in the shack controller rather than the rotor unit)   */


void displayStructure()
{
  Serial.println("R1 : " + String(Paket.R12));
  Serial.println("R3 : " + String(Paket.R34));
  //Serial.println("R5 : " + String(Paket.R5));
  Serial.println("R6 : " + String(Paket.R6));
  Serial.println("R7 : " + String(Paket.R7));
  //Serial.println("R8 : " + String(Paket.R8));
  Serial.println("R_cur_direct : " + String(Paket.R_cur_direct));
  Serial.println();

}

void validateValues()
{
  while ((Paket.RS12 != Paket.R12) | (Paket.RS34 != Paket.R34))
  {
    char Strup[17];
    celeste_fn(cur_direct);
    sprintf(Strup, "Rotor Err %4d %2s", cur_direct, celeste);
    lcd.setCursor(0, 1);
    lcd.print(Strup);
  }

  if ((Paket.RS6 != Paket.R6)| (Paket.RS7 != Paket.R7))
  {
   char Strup[17];
    celeste_fn(cur_direct);
    sprintf(Strup, "Coax Err  %3d %3d", Paket.RS6, Paket.RS7);
    lcd.setCursor(0, 1);
    lcd.print(Strup);
  }
}

// LORA LOOPS -- Send and Receive

void sendMessage(void) 
{
  Serial.println("Entered sendMessage loop; this is what's being sent: ");
  displayStructure();                   // show what's in the variable structure

  Wire.beginTransmission(42);                   // # = slave address
  Wire.write((uint8_t*) &Paket, sizeof(Paket)); // cast Paket to uint8_t [unsigned char]
  Wire.endTransmission();                     // finish packet and send it

  /*LoRa.beginPacket();                   // start packet
  LoRa.write((uint8_t*) &Paket, sizeof(Paket)); // cast Paket to uint8_t [unsigned char]
  LoRa.endPacket();                     // finish packet and send it
  */ 
  // msgCount++;                        // increment message ID
  
  Serial.println("Coming out ouf SendMessage Loop");
  Serial.println();
}
 

/* onReceive checks for a valid packet, reads it into the string "incoming" and converts
   the incoming string in the integer variable R_cur_direct; the rotor resistor value is 
   the only value transmitted from the rotor contoller. 
*/

void onReceive(int packetSize) 
{  
  uint8_t incomingLength = 0;
  String incoming; 
 	
	Serial.println("Entered Receive Loop; packet content:");
  displayStructure();
  Serial.println();

  //*packetSize = LoRa.parsePacket();

	/* if (packetSize) // Only read if there is some data to read..
 	{
   		LoRa.readBytes((uint8_t *)&Paket, packetSize);
 	}

  if (incomingLength != incoming.length())    // check length for error
	{
  	Serial.println("error: message length does not match length");
  	return;                             // skip rest of function
	}

  // print details to serial interface:
  Serial.println("Received package" );
	Serial.println("RSSI: " + String(LoRa.packetRssi()));
	Serial.println("Snr: "  + String(LoRa.packetSnr()));
  Serial.println("Packet Size: " + String(packetSize));
	displayStructure();                   // show what's in the variable structure
  */

   Wire.onReceive (receiveEvent);

  Serial.println("Coming out ouf Receive Loop");
  Serial.println();
}



// ================= M A I N   S E T U P   S E C T I O N =======================


void setup() 
{
  Serial.begin(9600);
 
  Serial.println("Serial OK"); delay(500);
  // initialize LCD
  lcd.init();
  lcd.backlight();
  Serial.println("LCD Initialized "); delay(500);
  
  //SPI LoRa pins
  // SPI.begin(13, 12, 11, 7);
  
  // SPI.begin(SCK, MISO, MOSI, SS);
  SPI.begin();
  LoRa.setSPIFrequency(SPIFREQ);       // sets frequency of SPI interface; default = 8MHz
  Serial.println("SPI started"); delay(500);

  //setup LoRa transceiver module
  LoRa.setPins(SS, RST, DIO0); // set SS, reset, IRQ pin
    // ss - new slave select pin to use, defaults to 10
    // reset - new reset pin to use, defaults to 9
    // dio0 - new DIO0 pin to use, defaults to 2. Must be interrupt capable via attachInterrupt(...).
 
  // LoRa.setSpreadingFactor(8);           // ranges from 6-12,default 7 see API docs
  LoRa.setTxPower(10);                   // sets tx power; default = 17dB; range = 2, 20
  // LoRa.setSyncWord(51153);

  Serial.println("LORA pins set"); delay(500);

  if (!Serial)
  {
    lcd.setCursor(0, 0);
    lcd.print("LoRa Duplex Init"); 
    Serial.println("Attempting LoRa Duplex Init"); delay(500);
    while(true);
  }

  displayStructure();

  byte cnt = 0;
  cnt = LoRa.begin(BAND);
  Serial.println("LoRa Begin Command Issued; Result: " + String(cnt)); delay(500);
 
  while ((cnt == 0))
  {
    Serial.println("LoRa Not Started; Result: " + String(cnt)); delay(500);
    digitalWrite(RST,HIGH); delay(300);
    digitalWrite(RST, LOW); delay(300);
    LoRa.begin(BAND); delay(300);
    cnt = LoRa.begin(BAND); 
  }

  /*if (!LoRa.begin(BAND)) 
  {    // set frequency of LORA chip 
    lcd.setCursor(0, 1);
    lcd.print("  LoRa Failed   "); 
    Serial.print("LoRa Failed"); delay(500);
    while (true);
  }
  */

  Wire.onReceive (receiveEvent);      // TEMP
 
  Serial.println("LoRa Running"); delay(300);

  lcd.clear(); 
  lcd.setCursor(0, 1);
  lcd.print("LoRa ini success");
  delay(5000);
  
 /* if (packetSize == 0) {
    lcd.setCursor(0, 1);
    lcd.print("Wait'g for Rotor");
    onReceive(LoRa.parsePacket());
    while(true);
  }
  else {
    lcd.setCursor(0, 1);
    lcd.print(" Rotor LoRa OK  ");
    delay(5000);
  }

  */
  lcd.clear();
  delay(3000);
}

 

// =============== M A I N   C O D E   S E C T I O N =====================


void loop() {

  // TEMP onReceive(LoRa.parsePacket());    // parse for a packet, and call onReceive with the result:

  // Read rotor turn knob (read through A0) value and set to 60 degree equivalent
  
  if haveData
  {
    displayStructure();
    haveData = false;
  }

  new_direct_fn();
  Serial.println("Determined new direction: " + String(new_direct));
  cur_direct_fn();
  Serial.println("Determined current direction" + String(cur_direct));

  //Serial.print("Cur Direction:\t" + (String)cur_direct + "\n");

  //Serial.print("Pot Dir:\t" + (String)analogRead(R_new_direct) + "\n");

  lcd_display();

  // Serial.println("-----------------------------------------");



  if (abs(cur_direct - new_direct) > 3) {
    turn_rotor_fn();
  }

  delay(50);
}
